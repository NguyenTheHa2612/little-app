import { Injectable } from '@angular/core';
import { CommonUtils } from '../../services/common-utils.service';
import { FormGroup } from '@angular/forms';

@Injectable()
export class BaseComponent {
  /**
   * Build FormGroup
   * @param formData value of controls
   * @param formConfig object formConfig
   * @param validateForm custom validate for formConfig
   */
  public buildForm(formData: any, formConfig: any, validateForm?: any): FormGroup {
    return CommonUtils.createForm(formData, formConfig, validateForm);
  }
}
