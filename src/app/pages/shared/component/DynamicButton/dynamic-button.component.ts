import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dynamic-button',
  templateUrl: './dynamic-button.component.html',
})
export class DynamicButtonComponent implements OnInit {
  @Input() name: string;
  constructor() { }

  ngOnInit(): void { }
}
