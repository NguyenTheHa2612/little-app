import { Injectable } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CommonUtils {
  public static createForm(formData: any, options: any, validate?: any): FormGroup {
    console.warn('formData', formData);
    console.warn('options', options);
    console.warn('validate', validate);
    const formGroup = new FormGroup({});
    // tslint:disable-next-line: forin
    for (const property in options) {
      console.warn('property>>>', property);
      if (formData.hasOwnProperty(property)) {
        options[property][0] = formData[property];
      }
      formGroup.addControl(property, new FormControl(options[property][0], options[property][1]));
    }
    if (validate) {
      formGroup.setValidators(validate);
    }

    return formGroup;
  }
}
