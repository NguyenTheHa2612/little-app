export const STATUS = [
  {id: 1, name: 'New'},
  {id: 2, name: 'In Progress'},
  {id: 3, name: 'Resolve'},
  {id: 4, name: 'Reject'},
  {id: 5, name: 'Pending'},
  {id: 6, name: 'Closed'},
];
