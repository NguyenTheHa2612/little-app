import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { STATUS } from '../../shared/constant';

@Component({
  selector: 'app-todo-search',
  templateUrl: './todo-form-search.component.html',
})
export class TodoFormSearchComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  add() {
    this.router.navigate(['/todo/add']);
  }
}
