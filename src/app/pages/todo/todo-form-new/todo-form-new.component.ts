import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { TodoService } from '../../services/todo.service';
import { STATUS } from '../../shared/constant';
import { BaseComponent } from '../../shared/component/base-component/base-component';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-todo-new',
  templateUrl: './todo-form-new.component.html',
})
export class TodoFormNewComponent extends BaseComponent implements OnInit {
  @ViewChild('largeModal') public largeModal: ModalDirective;
  public statusList = STATUS;
  public todoForm: FormGroup;
  public buildFormConfig = {
    subject: [''],
    decription: [''],
    date: [''],
    status: [null]
  };
  constructor(
    private todoService: TodoService
  ) {
    super();
    this.buildFormGroup({});
  }

  ngOnInit(): void {
    // this.todoService.openModal(this.largeModal);
  }

  private buildFormGroup(data?: any) {
    this.todoForm = this.buildForm(data, this.buildFormConfig);
  }
  onSubmit() {
    console.warn('todoForm', this.todoForm)
    this.todoService.save(this.todoForm.value).subscribe(() => {
      alert('Create successfully')
    });
  }
}
