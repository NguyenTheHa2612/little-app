// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoComponent } from './todo/todo.component';
import { TodoFormNewComponent } from './todo/todo-form-new/todo-form-new.component';
const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Todo'
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        component: TodoComponent,
        data: {
          title: 'List'
        }
      },
      {
        path: 'add',
        component: TodoFormNewComponent,
        data: {
          title: 'Add'
        }
      }
    ]
  }
];
@NgModule ({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class TodoRoutingModule {}
