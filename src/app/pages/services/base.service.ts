import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable()

export class BasicService {
  constructor(
    private httpClient: HttpClient
  ) {}
  public save(item: any): Observable<any> {
    const url = environment.serverUrl.hant;
    return this.postRequest(url, item);
  }

  public postRequest(url: string, data?: any): Observable<any> {
    return this.httpClient.post(url, data).pipe(
      tap(() => {
        alert('Add successfully');
      })
    );
  }
}
