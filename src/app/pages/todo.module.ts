// Angular
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { TodoRoutingModule } from './todo-routing.module';
import { TodoComponent } from './todo/todo.component';
import { TodoFormSearchComponent } from './todo/todo-form-seach/todo-form-search.component';
import { DynamicButtonComponent } from './shared/component/DynamicButton/dynamic-button.component';
import { TodoFormNewComponent } from './todo/todo-form-new/todo-form-new.component';
import { ModalModule } from 'ngx-bootstrap';
import { HttpClientModule } from '@angular/common/http';

// Todo routing

@NgModule ({
  imports: [
    CommonModule,
    FormsModule,
    TodoRoutingModule,
    ModalModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  declarations: [
    TodoComponent,
    TodoFormSearchComponent,
    DynamicButtonComponent,
    TodoFormNewComponent
  ]
})

export class TodoModule {}
